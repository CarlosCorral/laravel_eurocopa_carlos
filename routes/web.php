<?php

use App\Http\Controllers\GrupoController;
use App\Http\Controllers\JugadorController;
use App\Http\Controllers\PaisController;
use App\Http\Controllers\PartidoController;
use App\Http\Controllers\RestWebServicesController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [GrupoController::class,'inicio']);

Route::get('/grupos', [GrupoController::class,'index'])->name("grupos.index");

Route::get('/paises/{pais}', [PaisController::class,'show'])->name("paises.show");

Route::get('/paises/{partido}/disputar', [PartidoController::class,'update'])->name("partidos.update");

Route::get('/api/partido/{partido}/resetear', [RestWebServicesController::class,'resetear']);

Route::get('/api/partido/{pais}/{posicion}', [RestWebServicesController::class,'jugadores']);

Route::post('/jugadores/busquedaAjax', [JugadorController::class,'buscar']);