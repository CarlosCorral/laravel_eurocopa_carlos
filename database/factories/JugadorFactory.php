<?php

namespace Database\Factories;

use App\Models\Jugador;
use App\Models\Pais;
use Illuminate\Database\Eloquent\Factories\Factory;

class JugadorFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Jugador::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $posiciones=["POR","DEF","CEN","DEL"];
        $nombre=$this->faker->name('male');
        $fechaNaciemiento=$this->faker->date($format = 'Y-m-d', $max = 18);
        return [
            "nombre"=>$nombre,
            "numeroCamiseta"=>random_int(1,25),
            "fechaNacimiento"=>$fechaNaciemiento,
            "posicion"=>$posiciones[random_int(0,count($posiciones)-1)],
            'pais_id'=>Pais::all()->random()->id,
        ];
    }
}
