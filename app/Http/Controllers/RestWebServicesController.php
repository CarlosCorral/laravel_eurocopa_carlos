<?php

namespace App\Http\Controllers;

use App\Models\Pais;
use App\Models\Partido;
use Illuminate\Http\Request;

class RestWebServicesController extends Controller
{
    public function resetear($id){
        $partido=Partido::find($id);
        $partido->disputado=false;
        $partido->goles_pais1=0;
        $partido->goles_pais2=0;
        $partido->save();
        $paises=$partido->paises;
        return response()->json(["mensaje"=>"Se  ha reseteado el partido".$paises[0]."-".$paises[1]]);
    }

    public function jugadoresPosicion(Pais $pais,$posicion){
        foreach($pais->jugadores() as $jugador){
            if($posicion==$jugador->posicion)
                $jugadores[]=$jugador;
        }
        return response()->json(["jugadores"=>$jugadores]);
    }
}
