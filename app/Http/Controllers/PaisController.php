<?php

namespace App\Http\Controllers;

use App\Models\Pais;
use Illuminate\Http\Request;

class PaisController extends Controller
{
    public function show(Pais $pais){
        $partidos1=$pais->partidos1;
        $partidos2=$pais->partidos2;
        $puntos=0;
        foreach($partidos1 as $partido){
            if($partido->disputado!=0){
                if($partido->goles_pais1 > $partido->goles_pais2){
                    $puntos+=3;
                }if($partido->goles_pais1 == $partido->goles_pais2){
                    $puntos+=1;
                }
            }
        }
        foreach($partidos2 as $partido){
            if($partido->disputado!=0){
                if($partido->goles_pais2 > $partido->goles_pais1){
                    $puntos+=3;
                }if($partido->goles_pais1 == $partido->goles_pais2){
                    $puntos+=1;
                }  
            }
        }
        return view('paises.show',['puntos'=>$puntos,"pais"=>$pais]);
    }
}
