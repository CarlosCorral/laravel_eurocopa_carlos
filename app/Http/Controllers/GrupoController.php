<?php

namespace App\Http\Controllers;

use App\Models\Grupo;
use App\Models\Jugador;
use Illuminate\Http\Request;

class GrupoController extends Controller
{
    public function inicio(){
        return  redirect()->action([GrupoController::class,'index']);
    }

    public function index(){
        $grupos=Grupo::all();
        return view('grupos.index',['grupos'=>$grupos]);
    }


}
