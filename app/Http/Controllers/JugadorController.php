<?php

namespace App\Http\Controllers;

use App\Models\Jugador;
use Illuminate\Http\Request;

class JugadorController extends Controller
{
    public function buscar(Request $request){
        $jugadores=Jugador::where("numeroCamiseta","like","%".$request->buscador."%")->pluck("nombre");
        return response()->json($jugadores);
        }
}
