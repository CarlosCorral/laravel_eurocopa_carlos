<?php

namespace App\Http\Controllers;

use App\Models\Partido;
use Illuminate\Http\Request;

class PartidoController extends Controller
{
    public function update(Partido $partido){
        $partido->disputado=true;
        $partido->goles_pais1=random_int(0,4);
        $partido->goles_pais2=random_int(0,3);
        $partido->save(); 
    }

}
