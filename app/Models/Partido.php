<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Partido extends Model
{
    use HasFactory;
    protected $table="partidos";


    public function paises()
    {
    
        return $this->belongsTo(Pais::class,'pais1_id')->get()
        ->merge($this->belongsTo(Pais::class,'pais2_id')->get());
    }

    public function pais1()
    {
    
        return $this->belongsTo(Pais::class,'pais1_id')->get();
    }
    public function pais2()
    {
    
        return $this->belongsTo(Pais::class,'pais2_id')->get();
    }
}
