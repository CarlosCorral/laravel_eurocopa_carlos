@extends('layouts.master')
@section('titulo')
    Eurocopa 2020
@endsection
@section('contenido')

    <h2>Pais : {{$pais->nombre}} ( {{$puntos}} puntos)</h2>
    <h4>Partidos</h4>
    <?php 
        foreach($pais->partidos as $partido){
            echo '<div border="1px">';
            if($partido->disputado==false){
                echo $partido->pais1()." - ". $partido->pais2()." SIN DISPUTAR <br>";
                echo '<a type="button" class="btn btn-primary" href="{{ route("partidos.update" , $partido ) }}">Disputar</a>';
            }else{
                echo $partido->pais1()." ".$partido->goles_pais1." ".$partido->pais2()." ".$partido->goles_pais2;
            }
            echo "</div>";
        }
    ?>
   
    <table border="1px">
    <tr><th>Número</th><th>Nombre</th><th>Posición</th><th>Edad</th></tr>
    @foreach ( $pais->jugadores as $jugador)
    <tr><td>{{$jugador->numeroCamiseta}}</td><td>{{$jugador->nombre}}</td><td>{{$jugador->posicion}}</td><td></td></tr>
    @endforeach
    </table>  
   
@endsection