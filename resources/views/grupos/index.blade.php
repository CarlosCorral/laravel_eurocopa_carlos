@extends('layouts.master')
@section('titulo')
    Eurocopa 2020
@endsection
@section('contenido')
<div class="row">
    @foreach( $grupos as $grupo )
        <div>
            <h3>Grupo {{$grupo->letra}}</h3>
            <ul>
            @foreach ($grupo->paises as $pais)
            <li><a href="{{ route('paises.show' , $pais ) }}">{{$pais->nombre}}-
            <?php
                $contador=0;
                foreach($pais->partidos as $partido){
                    if($partido->disputado==true){
                        $contador++;
                    }
                } 
                echo $contador;
            ?>
            </a></li>
            @endforeach
            </ul>
        </div>
    @endforeach
</div>

@endsection